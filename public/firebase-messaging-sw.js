/* eslint-disable no-undef */
/* eslint-disable */
importScripts('https://www.gstatic.com/firebasejs/5.5.6/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.5.6/firebase-messaging.js');

firebase.initializeApp({'messagingSenderId': '158153948884'});
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  var notificationTitle = 'Background Message Title';
  var notificationOptions = {
    body: 'Background Message body.',
    icon: 'https://assets.gitlab-static.net/uploads/-/system/project/avatar/9845717/u_4215863670_261223381_fm_27_gp_0.jpg?width=64'
  };
  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});