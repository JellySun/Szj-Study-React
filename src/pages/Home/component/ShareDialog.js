import React from 'react';
import './ShareDialog.css';
// 定义组件的方法二: 函数定义组件
class ShareDialog extends React.Component {
  selectShareItem (val) {
    this.props.changeShareItemP(val);
  }
  render () {
    return (
      <div className={`share-dialog ${this.props.showShareDialog?'hoDownBlock':'hoDownNone'}`}>
        <ul className="sd-list">
          {this.props.shareList.map((val, index) => 
            <li className="sd-list-item" key={index}
            /* 这里需要改变 this 指向 */
            onClick={this.selectShareItem.bind(this, val)}>{val.title}
            </li>
          )}
        </ul>
      </div>
    )
  }
}
export default ShareDialog;