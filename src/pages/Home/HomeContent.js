import React from 'react';
import './HomeContent.css';
export default class HomeContent extends React.Component {
  render () {
    return (
      <div className="home-content">
        <div className="hc-cont flex align-start">
          <div className="hcc-left">
            <section className="hccl-cont sp24">内容区域</section>
            <div className="hccl-comment">
              <section className="hcclc-cont sp24">
                评论区域
              </section>
            </div>
            <section className="hccl-recommend sp24">推荐区域</section>
          </div>
          <div className="hcc-right">广告区域</div>
        </div>
      </div>
    )
  }
}