/*
 * @Author: sunzhongjie
 * @Date: 2021-09-02 16:05:15
 * @LastEditors: Jelly
 * @LastEditTime: 2022-02-17 14:01:09
 */
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import App from '../pages/App.js';
import RouteFallback from './RouteFallback.js';
import Home from '@/pages/Home';
import Analyse from '@/pages/analyse';

const BasicRoute = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={App} />
        <Route path="/home" component={Home} />
        <Route path="/analyse" component={Analyse} />
        <Route component={RouteFallback} />
      </Switch>
    </Router>
  );
}
export default BasicRoute;
