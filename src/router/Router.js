/*
 * @Author: sunzhongjie
 * @Date: 2021-09-02 16:05:15
 * @LastEditors: Jelly
 * @LastEditTime: 2022-02-17 14:03:23
 */
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Home from '@/pages/Home';
import Center from '@/pages/Center';
import Analyse from '@/pages/analyse';
ReactDOM.render (
  <Router>
    <div>
      <Route exact path="/" component={Home} ></Route>
      <Route path="/center" component={Center} ></Route>
      <Route path="/analyse" component={Analyse} ></Route>
    </div>
  </Router>
)