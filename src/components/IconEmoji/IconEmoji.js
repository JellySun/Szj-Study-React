import React from 'react';
import { emojify } from 'react-emojione';
import './IconEmoji.css';
const emojiList1 = [
  ':smile:', ':smiley:',':wink:', ':relaxed:',':heart_eyes:',':kissing_heart:',':kissing_closed_eyes:',':flushed:',':grin:',':relieved:',
  ':unamused:',':stuck_out_tongue_winking_eye:',':stuck_out_tongue_closed_eyes:',':smirk:',':sweat:',':pensive:',':confounded:',':disappointed_relieved:',':cold_sweat:',':blush:',
  ':fearful:',':persevere:',':cry:',':sob:',':joy:',':scream:',':angry:',':sleepy:',':mask:',':worried:',
  ':innocent:',':yum:',':anguished:',':frowning:',':hushed:',':dizzy_face:',':stuck_out_tongue:',':no_mouth:',':sunglasses:',':sweat_smile:',
  ':+1:',':-1:',':clap:',':v:',':pray:',':fist:',':heart:',':broken_heart:',':heartbeat:',':sparkling_heart:',
]
const emojiList2 = [
  ':cupid:',':beer:',':beers:',':birthday:',':exclamation:',':bangbang:',':interrobang:',':underage:',':no_bicycles:',':no_mobile_phones:',
  ':u7981:',':up:',':sunny:',':crescent_moon:',':high_brightness:',':first_quarter_moon_with_face:',':zap:',':snowflake:',':cloud:',':tada:',
  ':bear:',':cat:',':cow:',':dog:',':hamster:',':monkey_face:',':rabbit:',':tiger:',':turtle:',':whale:',
  ':whale2:',':dolphin:',':crocodile:',':dragon_face:',':chipmunk:',':hatching_chick:',':hatched_chick:',':baby_chick:',':frog:',':ant:',
  ':bug:',':beetle:',':ghost:',':accept:',':airplane:',':alarm_clock:',':ambulance:',':angel:',':apple:',':arrows_counterclockwise:',
]
const emojiList3 = [
  ':balloon:',':beginner:',':bikini:',':black_nib:',':blossom:',':bomb:',':boom:',':man:',':bread:',':bulb:',
  ':cake:',':cactus:',':camera:',':candy:',':checkered_flag:',':cherries:',':cherry_blossom:',':chocolate_bar:',':christmas_tree:',':clapper:',
  ':closed_umbrella:',':closed_lock_with_key:',':clubs:',':cocktail:',':coffee:',':confetti_ball:',':crown:',':dancer:',':woman:',':dart:',
  ':doughnut:',':first_quarter_moon:',':fries:',':game_die:',':golf:',':guitar:',':gun:',':herb:',':hibiscus:',':high_heel:',
  ':knife:',':icecream:',':ideograph_advantage:',':jack_o_lantern:',':key:',':kiss:',':lock:',':lollipop:',':mag:',':moneybag:',
]
const emojiList4 = [
  ':bell:',':no_bell:',':ribbon:',':skull:',':snowman:',':spaghetti:',':sparkles:',':strawberry:',':sunflower:',':sweat_drops:',
  ':toilet:',':watermelon:',':anger:',':chart:',':corn:',':deciduous_tree:',':dash:',':dress:',':ear_of_rice:',':eyes:',
  ':fallen_leaf:',':footprints:',':fishing_pole_and_fish:',':poop:',':heavy_check_mark:',':leaves:',':lipstick:',':mag_right:',':mailbox_with_mail:',':mailbox_with_no_mail:',
  ':man_with_gua_pi_mao:',':metal:',':mushroom:',':musical_keyboard:',':on:',':arrow_left:',':arrow_up:',':arrow_down:',':arrow_right:',':atm:',
  ':crystal_ball:',':smile_cat:',':smiley_cat:',':joy_cat:',':heart_eyes_cat:',':smirk_cat:',':kissing_cat:',':scream_cat:',':crying_cat_face:',':pouting_cat:',
]

class IconEmoji extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      selectIconPage: 1,
      options: {
        convertShortnames: true,
        convertUnicode: true,
        convertAscii: true,
        style: {
          // backgroundImage: 'url("/path/to/your/emojione.sprites.png")',
          height: 26,
          margin: 3,
          cursor: 'pointer'
        },
        onClick: event => {
          this.props.selectEmojiP(event.target.title);
        }
      }
    }
  }
  switchIconPage (page) {
    this.setState(() => {
      return { selectIconPage: page }
    })
  }
  
  render () {
    return (
      <div className={`emoji-box borderHornA absolute ${this.props.showEmojiDialog?'soDownBlock':'soDownNone'}`}>
        <ul className="emoji-ul clear">
          {emojiList1.map((item, index) =>
            <li className="emoji-li flex" key={index}
            style={{display: this.state.selectIconPage === 1?'block':'none'}}>{emojify(item, this.state.options)}</li>
          )}
          {emojiList2.map((item, index) =>
            <li className="emoji-li flex" key={index}
            style={{display: this.state.selectIconPage === 2?'block':'none'}}>{emojify(item, this.state.options)}</li>
          )}
          {emojiList3.map((item, index) =>
            <li className="emoji-li flex" key={index}
            style={{display: this.state.selectIconPage === 3?'block':'none'}}>{emojify(item, this.state.options)}</li>
          )}
          {emojiList4.map((item, index) =>
            <li className="emoji-li flex" key={index}
            style={{display: this.state.selectIconPage === 4?'block':'none'}}>{emojify(item, this.state.options)}</li>
          )}
        </ul>
        <div className="emoji-dotes flex">
          {[1,2,3,4].map((item, index) =>
            <div className={`emoji-dotes-item ${this.state.selectIconPage === item?'emoji-dotes-item-active':''}`}
            key={index}
            onClick={this.switchIconPage.bind(this, item)}></div>
          )}
        </div>
      </div>
    )
  }
}
export default IconEmoji;