import React from 'react';
import { NavLink } from 'react-router-dom';
import './NavBar.css';

const NavBar = () => {
  return (
    <div>
      <NavLink exact to='/home' className='white' activeClassName='active' >Home</NavLink>
      <NavLink to='/center' className='white' activeClassName='active' >Center</NavLink>
      <NavLink to='/redirect' className='white' activeClassName='active' >Redirect</NavLink>
      <NavLink to='/404' className='white' activeClassName='active' >404</NavLink>
    </div>
  )
}
export default NavBar;
