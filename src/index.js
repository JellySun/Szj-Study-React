/*
 * @Author: sunzhongjie
 * @Date: 2021-09-02 16:05:15
 * @LastEditors: Jelly
 * @LastEditTime: 2022-02-17 14:07:47
 */
// eslint-disable-next-line
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';

import './index.css';
import '@/assets/css/color.css';
import '@/assets/css/class.css';
import '@/assets/css/reset.css';
import '@/assets/css/animate.css';
import '@/assets/css/rewriteUI.css';
import '@/assets/iconfont/iconfont.css';
import App from '@/pages/App';
import Home from '@/pages/Home/index';
import Center from '@/pages/Center/index';
import Analyse from '@/pages/analyse/index';
import ErrorPage from '@/pages/404.js';

/* Switch 设置404页面, Router 路由器, Route 路由 */
ReactDOM.render (
  /* basename 是增加一层路由目录,ep: /one/home */
  // <Router basename="one">
  <Router>
    <div>
      <Switch>
        <Route exact path="/" component={App} />
        <Route path="/home" component={Home} />
        <Route path="/center" component={Center} />
        <Route path="/analyse" component={Analyse} />
        <Redirect from="/redirect" to="/home" />
        {/* 这个一定要在所有链接最后 */ }
        <Route component={ErrorPage} />
      </Switch>
    </div>
  </Router>, document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
